// index.js

// BASE SETUP
// ==============================================

const express = require('express');
const router = express.Router();
const app = express();
const port = 9999;
var project1Controller = require('./app/project1/project1-controller');
var project2Controller = require('./app/project2/project2-controller');


// ROUTES
// ==============================================

router.get('/', project1Controller.index);
router.get('/project2', project2Controller.index);


// START THE SERVER
// ==============================================

module.exports = router;
app.listen(port);
app.use('/', router);