// project2_controller.js

var async = require('async');
var requestModule = require('request');
var requests = [{
	url: 'https://join.reckon.com/test2/textToSearch'
}, {
	url: 'https://join.reckon.com/test2/subTexts'
}];

exports.index = function(request, response) {

	async.map(requests, function(obj, callback) {
		requestModule(obj, function(error, response, body) {
			if (!error && response.statusCode == 200) {
				var body = JSON.parse(body);
				callback(null, body);
			} else {
				callback(error || response.statusCode);
			}
		});
	}, function(err, results) {
		if (err) {
			response.send("Too many tries, cannot access the APIs.");
		} else {

			var textToSearch = results[0];
			var subTexts = results[1];

			var searchString = '';
			var output = {
				candidate: 'Valentin',
				text: 'Peter told me (actually he slurrred) that peter the pickle piper piped a pitted pickle before he petered out. Phew!',
				results: []
			};
			var position = [];
			subTexts.subTexts.forEach(function(element) { 
				position = [];

				for(var i = 0; i < textToSearch.text.length; i++) {

					searchString = '';

					for (y = i; y < i + element.length; y++) {
						searchString += textToSearch.text.charAt(y);
					}

					if ( searchString.toLowerCase() == element.toLowerCase() ) {
						position.push(i+1);
					}

				}

				if (position.length == 0) {
					position.push('<No Output>');
				}

				output.results.push({
					subtext: element,
					result: position + ""
				});

			});

			var options = {
				method: 'POST',
				uri: 'https://join.reckon.com/test2/submitResults',
				body: {
					'result': JSON.stringify(output)
				},
				json: true 
			};


			requestModule(options, function (error, response, body) {
				if (!error && response.statusCode == 200) {
					console.log(body.result);
				} else {
					console.log('There has been an error submitting the result.');
				}
			});



		}
	});
	
};