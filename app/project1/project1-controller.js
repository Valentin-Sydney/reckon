// project1_controller.js

var async = require('async');
var requestModule = require('request');
var requests = [{
	url: 'https://join.reckon.com/test1/rangeInfo'
}, {
	url: 'https://join.reckon.com/test1/divisorInfo'
}];

exports.index = function(request, response) {

	async.map(requests, function(obj, callback) {
		requestModule(obj, function(error, response, body) {
			if (!error && response.statusCode == 200) {
				var body = JSON.parse(body);
				callback(null, body);
			} else {
				callback(error || response.statusCode);
			}
		});
	}, function(err, results) {
		if (err) {
			response.send("Too many tries, cannot access the APIs.");
		} else {

			var rangeInfo = results[0];
			var divisorInfo = results[1];
			var output = '';

			divisorInfo.outputDetails.forEach(function(element, index) {
				output += index + ': ';
				if (rangeInfo.lower % element.divisor == 0) {
					output += element.output;
				}
				if (rangeInfo.upper % element.divisor == 0) {
					output += element.output;
				}
				output += '<br />';
			});
			
			response.send(output);
		}
	});
	
};
